# Calendar App - Qubit Programming Challenge

A simple event calendar that represents a 12 hour snippet of a day with events
placed on the calendar based on the time frame they occupy.

Rules:
 - No events can overlap
 - If two events or more collide they must have the same width
 - Calendar is 620px wide (10px of padding left/right), and 720px high
 - Events should pay attention to Calendar padding

## Positioning Methodology

Calendar is 720px in height, there are 12 hours in this day snippet (9am - 9pm) which leads each row to be 60px in height (30px in height for half hour sections). Events from 9am are 0 indexed where 0 = 9am, 30 = 9:30 etc.
Events can therefore be positioned absolutely over the calendar using position top = start time.
Event height is determined by event end time (end - start = height).
Events which collide are split into groups (e.g. events 1 & 2 may collide, but not with events 4 & 5) and their widths adjusted based on the calendars overall width and how many events are colliding. Each collided events position left is offset against the last finishing position of the last collided event which leads to cascading events.

## Exposed Functions

For changing the events, in browser console:
```javascript
layOutDay([/* events here*/]);
```

For checking the current state of the application:
```javascript
showStore();
```

## Building

Obligatory
```
$ npm install
```

### Dev
Requires two console tabs. In first tab:
```
$ npm run start
```
In second tab:
```
$ npm run Dev
```
Navigate to `localhost:8080`. Webpack will rebuild changes live via hot reload

### Prod
For production bundle build
```
$ npm run build && npm run start
```
Bundled file output in `/dist`. Navigate to `localhost:4060` or alternately copy `view/index.html` to `/dist` and open `index.html`
