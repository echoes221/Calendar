import { expect } from 'chai';
import eventsReducer from 'reducers/events';

describe('eventsReducer', function () {
    describe('initalState', function () {
        it('should return the intital state untouched', function () {
            expect(eventsReducer(undefined, {})).to.deep.equal([
                { start: 30, end: 150 },
                { start: 540, end: 600 },
                { start: 560, end: 620 },
                { start: 610, end: 670 }
            ]);
        });
    });

    describe('ADD_EVENTS', function () {
        it('should handle ADD_EVENTS', function () {
            expect(eventsReducer([], {
                type: 'ADD_EVENTS',
                events: [1, 2, 4]
            })).to.deep.equal([1, 2, 4]);
        });
    });

    describe('Unrecognised type', function () {
        it('should return the current state if action not recognised', function () {
            expect(eventsReducer([], {
                type: 'RANDOM_EVENT',
                foo: [1, 2, 3]
            })).to.deep.equal([]);
        });
    });
});
