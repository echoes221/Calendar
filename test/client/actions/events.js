import { expect } from 'chai';
import { addEvents } from 'actions/events';

describe('Event actions', function () {
    describe('addEvents', function () {
        expect(addEvents([1, 2, 3])).to.deep.equal({
            type: 'ADD_EVENTS',
            events: [1, 2, 3]
        });
    });
});
