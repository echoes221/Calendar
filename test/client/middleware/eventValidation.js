import { expect } from 'chai';
import eventValidation from 'middleware/eventValidation';

const createFakeStore = fakeData => ({
    getState () {
        return fakeData;
    }
});

const dispatchWithStoreOf = (storeData, action) => {
    let dispatched = null;
    const dispatch = eventValidation(createFakeStore(storeData))(actionAttempt => dispatched = actionAttempt);
    dispatch(action);
    return dispatched;
};

describe('eventValdation middleware', function () {
    describe('eventValidation', function () {
        it('should continue flow if event is not ADD_EVENTS', function () {
            const action = {
                type: 'RANDOM_ACTION'
            };

            expect(dispatchWithStoreOf({}, action)).to.deep.equal(action);
        });

        it('should not continue the flow if the events are incorrectly formatted', function () {
            const action = {
                type: 'ADD_EVENTS',
                events: [{start: 40, end: 30}]
            };

            expect(dispatchWithStoreOf({}, action)).to.be.null;
        });

        it('should filter the events by start time before continuing', function () {
            const action = {
                type: 'ADD_EVENTS',
                events: [{start: 40, end: 45}, {start: 30, end: 40}]
            };

            expect(dispatchWithStoreOf({}, action)).to.deep.equal({
                type: 'ADD_EVENTS',
                events: [{start: 30, end: 40}, {start: 40, end: 45}]
            });
        });
    });
});
