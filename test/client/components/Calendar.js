import React from 'react';
import { expect } from 'chai';
import { render, mount } from 'enzyme';
import Calendar from 'components/Calendar';

describe('<Calendar />', function () {
    it('should be able to render', function () {
        const component = render(<Calendar times = {[]} />);
        expect(component.find('.calendarWrapper').length).to.equal(1);
    });

    it('should be able to render children as a function', function () {
        const component = mount(
            <Calendar times = {[]}>
                {() => (<div className = 'childComponent' />)}
            </Calendar>
        );

        expect(component.find('.childComponent').length).to.equal(1);
    });

    it('should pass an object to the child function', function () {
        let passedValue;
        const component = mount(
            <Calendar times = {[]}>
                {(rect) => {
                    passedValue = rect;
                }}
            </Calendar>
        );

        expect(passedValue).to.be.an.object;
    });

    it('should contain time slot component', function () {
        const component = render(<Calendar times = {[1]} />);
        expect(component.find('.timeSlotWrapper').length).to.equal(1);
    });

    it('should contain the interval component', function () {
        const component = render(<Calendar times = {[1]} />);
        expect(component.find('.interval').length).to.equal(1);
    });
});
