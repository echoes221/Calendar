import React from 'react';
import { expect } from 'chai';
import { render, mount } from 'enzyme';
import EventCard from 'components/EventCard';

describe('<EventCard />', function () {
    it('should be able to render', function () {
        const component = render(<EventCard />);
        expect(component.find('.eventWrapper').length).to.equal(1);
    });

    it('should be able to accept width, height, top, left props', function () {
        const props = { width: 10, height: 10, top: 20, left: 50};
        const component = mount(<EventCard {...props} />);

        expect(component.props()).to.deep.equal(props);
    });
});
