import { expect } from 'chai';
import {
    extendEvents,
    doesEventCollideY,
    getCollisionGroups,
    getNoCollideMap,
    positionCollisionsInEvents,
    getBestCollidedWidth,
    positionEvents
} from 'utils/position';

describe('position', function () {
    beforeEach(function () {
        this.events = [
            { start: 300, end: 350 },
            { start: 400, end: 500 },
            { start: 450, end: 550 },
            { start: 500, end: 600 },
            { start: 650, end: 700 },
            { start: 670, end: 850 }
        ];

        this.rect = {
            left: 0,
            width: 500
        };

    });

    describe('extendEvents', function () {
        it('should add top, left, height, width to each event object', function () {
            expect(extendEvents([this.events[0]], this.rect)).to.deep.equal([{
                left: 0,
                width: 500,
                height: 50,
                top: 300,
                start: 300,
                end: 350
            }]);
        });
    });

    describe('doesEventCollideY', function () {
        it('should return false if events don\'t collide with each other', function () {
            expect(doesEventCollideY(this.events[0], this.events[2])).to.be.false;
        });

        it('should return true if events collide with each other', function () {
            expect(doesEventCollideY(this.events[1], this.events[2])).to.be.true;
        });
    });

    describe('getCollisionGroups', function () {
        it('should return an empty array if no events collide with each other', function () {
            expect(getCollisionGroups([
                { start: 50, end: 100 },
                { start: 150, end: 200}
            ])).to.be.empty;
        });

        it('should return a two dimensional array if events collide with each other in sequence', function () {
            expect(getCollisionGroups(this.events)).to.deep.equal([
                [1, 2, 3],
                [4, 5]
            ]);
        });
    });

    describe('getNoCollideMap', function () {
        it('should update the event noCollideIndex if an event can be re-positioned', function () {
            const events = extendEvents([
                { start: 50, end: 60 },
                { start: 55, end: 70 },
                { start: 61, end: 70 }
            ], this.rect);
            const collisionGroups = getCollisionGroups(events);

            expect(getNoCollideMap(events, collisionGroups)).to.deep.equal({
                2: 0
            });
        });
    });

    describe('positionCollisionsInEvents', function () {
        it('should not re-position any events if there are no collisions', function () {
            const noCollideEvents = [
                { start: 50, end: 100 },
                { start: 150, end: 200}
            ];
            const rect = this.rect;
            const events = extendEvents(noCollideEvents, this.rect);
            const extendedCopy = extendEvents(noCollideEvents, this.rect);
            const collisionGroups = getCollisionGroups(events);
            const noCollideMap = {};

            expect(positionCollisionsInEvents({
                events,
                collisionGroups,
                noCollideMap,
                rect
            })).to.deep.equal(extendedCopy);
        });

        it('should reposition any collided events, and set even widths to events', function () {
            const rect = this.rect;
            const events = extendEvents(this.events, rect);
            const collisionGroups = getCollisionGroups(events);
            const noCollideMap = {};
            const newlyPositioned = positionCollisionsInEvents({
                events,
                collisionGroups,
                noCollideMap,
                rect
            });

            describe('collided events', function () {
                collisionGroups.forEach((groups) => {
                    const eventWidth = Math.round(rect.width / groups.length);

                    groups.forEach((eventIndex, groupIndex) => {
                        const event = newlyPositioned[eventIndex];
                        const eventLeft = rect.left + (groupIndex * eventWidth);

                        it(`event ${eventIndex} should have a width of ${eventWidth} and a left position of ${eventLeft}`, function () {
                            expect(event.width).to.equal(eventWidth);
                            expect(event.left).to.equal(eventLeft);
                        });
                    });
                });
            });
        });
    });

    describe('getBestCollidedWidth', function () {
        it('should return the width evenly divided by collision group length if there are no collided index\'s', function () {
            const rect = this.rect;
            const extended = extendEvents(this.events, rect);
            const collisionGroup = getCollisionGroups(extended)[0];
            const noCollideMap = {};

            expect(getBestCollidedWidth(extended, collisionGroup, noCollideMap, rect.width)).to.equal(Math.round(rect.width / 3));
        });

        it('should return the width dividely evenly by collision group length minus collided index', function () {
            const rect = this.rect;
            const extended = extendEvents(this.events, rect);
            const collisionGroup = getCollisionGroups(extended)[0];
            const noCollideMap = {1: 0};

            expect(getBestCollidedWidth(extended, collisionGroup, noCollideMap, rect.width)).to.equal(Math.round(rect.width / 2));
        });
    });

    describe('positionEvents', function () {
        it('should return an array of positioned events', function () {
            const positionedEvents = positionEvents(this.events, this.rect);

            expect(positionedEvents.length).to.equal(this.events.length);
        });
    });
});
