import { expect } from 'chai';
import expose from 'utils/expose';
import store from 'store';

describe('expose', function () {
    describe('layOutDay', function () {
        it('should be available on the global instance', function () {
            expect(window.hasOwnProperty('layOutDay')).to.be.true;
        });

        it('should update event store when called', function () {
            window.layOutDay([{start: 0, end: 10 }]);

            expect(store.getState()).to.deep.equal({ events: [ { start: 0, end: 10 } ] });
        });
    });

    describe('showStore', function () {
        it('should be available on the global instance', function () {
            expect(window.hasOwnProperty('showStore')).to.be.true;
        });

        it('should return the store object when called', function () {
            expect(window.showStore()).to.be.an.object;
        });
    });
});
