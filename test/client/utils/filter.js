import { expect } from 'chai';
import { eventSort, eventsEndBeforeStart, isOdd } from 'utils/filter';

describe('filter', () => {
    beforeEach(function () {
        this.events = [
            { start: 400, end: 500 },
            { start: 500, end: 600 },
            { start: 300, end: 500 }
        ];
    });

    describe('eventSort', () => {
        it('should filter events by start date', function () {
            expect(eventSort(this.events)).to.deep.equal([
                { start: 300, end: 500 },
                { start: 400, end: 500 },
                { start: 500, end: 600 }
            ]);
        });
    });

    describe('eventsEndBeforeStart', function () {
        it('should return false if no events end before start', function () {
            expect(eventsEndBeforeStart(this.events)).to.be.false;
        });

        it('should return true if an event ends before start', function () {
            expect(eventsEndBeforeStart(this.events.push({start: 50, end: 40}))).to.equal.true;
        });
    });

    describe('isOdd', function () {
        it('should return true if a value is odd', function () {
            expect(isOdd(1)).to.be.true;
        });

        it('should return false if a value is not odd', function () {
            expect(isOdd(2)).to.be.false;
        });
    });
});
