'use strict';

import './styles/interval.css'

import React, { Component, PropTypes } from 'react';

const Intervals = ({ times }) => (
    <div>
        {times.map((time, index) => (
            <div className = 'interval' key = {index}></div>
        ))}
    </div>
);

export default Intervals;
