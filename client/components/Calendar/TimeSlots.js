'use strict';

import './styles/timeSlot.css'

import React, { Component, PropTypes } from 'react';
import { isOdd } from 'utils/filter';

/**
 * Time slots
 * Replicate some 'nthChild' magic as CSS isn't playing nice with react
 *
 * @component TimeSlots
 */
const TimeSlots = ({ times }) => (
    <div className = 'timeSlotContainer'>
        {times.map((time, index) => (
            <div key = {index} className = 'timeSlotWrapper'>
                <div className = {`timeLabels ${isOdd(index) ? 'smallLabel' : '' }`}>{time}</div>
            </div>
        ))}
    </div>
);

export default TimeSlots;
