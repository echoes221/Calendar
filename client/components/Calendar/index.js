'use strict';

import './styles/calendarIndex.css';

import React, { Component, PropTypes } from 'react';
import { keysIn } from 'lodash';
import TimeSlots from './TimeSlots';
import Intervals from './Intervals';

/**
 * Calendar Function as Child Component
 * Passes it's own (modifed) boundingClientRect down to children once
 * component has mounted
 * Expects children to be absolutely positioned on the event column
 *
 * @class Calendar
 */
class Calendar extends Component {
    constructor (props) {
        super(props);

        this.state = {
            rect: {}
        };
    }

    componentDidMount () {
        const { eventsColumn } = this.refs;
        const { width } = eventsColumn.getBoundingClientRect();

        // Simiulate padding in rect
        this.setState({
            rect: {
                width: width - 20,
                left: 10
            }
        });
    }

    render () {
        const { children, times } = this.props;
        const { rect } = this.state;

        return (
            <div className = 'calendarWrapper'>
                <div className = 'timeColumn'>
                    <TimeSlots times = {times} />
                </div>
                <div className = 'eventsColumn' ref = 'eventsColumn'>
                    <Intervals times = {times} />
                    {keysIn(rect).length ? children(rect) : null}
                </div>
            </div>
        );
    }
}

Calendar.propTypes = {
    children: PropTypes.func.isRequired,
    times: PropTypes.array.isRequired
};

export default Calendar;
