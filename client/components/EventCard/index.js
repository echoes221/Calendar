'use strict';

import './styles/eventCardIndex.css';

import React, { PropTypes } from 'react';
import { assign } from 'lodash';

/**
 * EventCard component
 *
 * @component EventCard
 * @props {Object}
 */
const EventCard = ({ width, height, top, left}) => (
    <div className = 'eventWrapper'
        style = {{ width, height, top, left}}>
        <div className = 'eventCard'>
            <div className = 'eventSliver'></div>
            <div className = 'eventTextWrapper'>
                <div className = 'eventText'>Sample item</div>
                <div className = 'eventSubText'>Sample location</div>
            </div>
        </div>
    </div>
);

EventCard.propTypes = {
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    height: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    top: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    left: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

export default EventCard;
