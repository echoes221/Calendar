'use strict';

const initialState = [
    { start: 30, end: 150 },
    { start: 540, end: 600 },
    { start: 560, end: 620 },
    { start: 610, end: 670 }
];

const eventsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_EVENTS':
            return action.events;
        default:
            return state;
    }
};

export default eventsReducer;
