'use strict';

/**
 * Injection point of time-app
 */

// core
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

// app
import store from './store';
import App from './app';
import expose from 'utils/expose';

render(
    <Provider store = {store}>
        <App />
    </Provider>,
    document.getElementById('time-app')
);
