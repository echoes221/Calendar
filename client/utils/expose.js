'use strict';

import { addEvents } from 'actions/events';
import store from 'store';
import { forOwn } from 'lodash';
const { dispatch } = store;

/**
 * Expose module, any functions, values etc that are
 * set to the exposed object below get set to the global window
 * object. Writing to the global instance isn't best practice, but needed
 * for this exercise. Be careful not to overwrite any standard globals...
 *
 * @module exposed
 */
const exposed = {};

/**
 * Accepts an array of events (each event is an object containing {start: val, end: val})
 * Connects directly to the store and dispatches the necessary event
 *
 * @for exposed
 * @method layOutDay
 * @param {Array} events
 */
exposed.layOutDay = (events) => {
    dispatch(addEvents(events));
};

/**
 * Returns the current state of the redux store for development purposes
 * TODO: Wrap in NODE_ENV = DEV check
 *
 * @for exposed
 * @method showStore
 * @return {Object}
 */
exposed.showStore = () => {
    return store.getState();
};

/**
 * Take any values that are stored in 'exposed' and write them to the window
 */
forOwn(exposed, (func, key) => window[key] = func);

export default window;
