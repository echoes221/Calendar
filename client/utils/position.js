'use strict';

import { assign, last, includes, isNull, get, has } from 'lodash';

/**
 * Takes raw [{start, end}] array and works out correct positioning to place
 * within the calendar. Works out event collisions and extends the original objects in
 * array to be akin to boundingClientRect object for later use by the eventcard for positioning
 *
 * @function positionEvents
 * @param {Array} rawEvents
 * @param {Object} rect container rect
 * @return {Array}
 */
export const positionEvents = (rawEvents, rect) => {
    const events = extendEvents(rawEvents, rect);
    const collisionGroups = getCollisionGroups(events);
    const noCollideMap = getNoCollideMap(events, collisionGroups);

    return positionCollisionsInEvents({
        events,
        collisionGroups,
        noCollideMap,
        rect
    });
};

/**
 * Extends an event array of objects to carry additional rect properties
 *
 * @function extendEvents
 * @param {Array} events
 * @param {Object} rect
 * @return {Array}
 */
export const extendEvents = (events, rect) => events.map(event => assign({}, event, {
    top: event.start,
    left: rect.left,
    height: event.end - event.start,
    width: rect.width
}));

/**
 * Checks if two events collide on the Y axis (via their start and end time)
 * returns truthy if so
 *
 * @function doesEventCollideY
 * @param {Object} event1
 * @param {Object} event2
 * @return {Boolean}
 */
export const doesEventCollideY = (event1, event2) => {
    const { start: start1, end: end1 } = event1;
    const { start: start2, end: end2 } = event2;

    return (start1 >= start2 && start1 <= end2) ||
        (end1 >= start2 && end1 <= end2);
};

/**
 * Checks if any events collide and returns a two-dimensional array
 * of events that collide with each other (by index)
 * e.g. if events 1, 2, 3 collide as well as events 5 & 6 it returns [[1,2,3], [5,6]]
 *
 * @function getCollisionGroups
 * @param {Array} events
 * @return {Array}
 */
export const getCollisionGroups = events => events.reduce((groups, event, index, events) => {
    // Check current index against last index for collisions
    if (index && doesEventCollideY(event, events[index - 1])) {
        // If last index matches the last index in the previous array push it
        // to previous array. Else, it's a new grouping so create a new array group
        if (last(last(groups)) === index -1) {
            groups[groups.length - 1].push(index);
        } else {
            groups.push([index -1, index]);
        }
    }

    return groups;
}, []);

/**
 * Finds events that don't collide with previous events in the collisionGroups array
 * Generates a hash map of {event index: collision index} where an available slot
 * is free to place the event
 *
 * @function findNoCollideIndex
 * @param {Array} events
 * @param {Array} collisionGroups
 * @return {Object} noCollideMap
 */
export const getNoCollideMap = (events, collisionGroups) => collisionGroups.reduce((noCollideMap, group) => {
    const noCollideHistory = [];

    group.forEach((eventIndex, index) => {
        if (!index) { return; }

        const event = events[eventIndex];

        // Check event against previous events (up to itself) and discover
        // if they collide or not. If they don't write that index to the map
        for (var i = 0; i < index; i++) {
            if (!includes(noCollideHistory, i) && !doesEventCollideY(event, events[group[i]])) {
                noCollideHistory.push(i);
                noCollideMap[eventIndex] = i;
                break;
            }
        }
    });

    return noCollideMap;
}, {});

/**
 * Takes all current events and adjusts positions and sizes so that any collided
 * events no longer collide with each other. Returns back full rebuild events
 *
 * @function positionCollisionsInEvents
 * @param {Object} options
 * @param {Array} events
 * @param {Array} collisionGroups 2D Array
 * @param {Object} noCollideMap
 * @param {Object} rect container rect
 * @return {Array} events
 */
export const positionCollisionsInEvents = (options) => {
    const { events, collisionGroups, noCollideMap, rect } = options;

    collisionGroups.forEach((group) => {
        // Collided events must have equal size
        const eventWidth = getBestCollidedWidth(events, group, noCollideMap, rect.width);

        // Apply new width & left to each of the events that have been collided
        // with
        group.forEach((eventIndex, groupIndex) => {
            const event = events[eventIndex];
            const index = get(noCollideMap, eventIndex, groupIndex);

            event.width = eventWidth;
            event.left = rect.left + (index * eventWidth);
        });
    });

    return events;
};

/**
 * Finds best width for each event based on collisions and moved collided events
 *
 * @function getBestCollidedWidth
 * @param {Array} events
 * @param {Array} collisionGroup
 * @param {Number} width
 * @return {Number} width
 */
export const getBestCollidedWidth = (events, collisionGroup, noCollideMap, width) => {
    let countOfMoved = 0;

    collisionGroup.forEach((eventIndex) => {
        if (has(noCollideMap, eventIndex)) {
            ++countOfMoved;
        }
    });

    return Math.round(width / (collisionGroup.length - countOfMoved));
};
