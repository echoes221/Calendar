'use strict';

import { some } from 'lodash';

/**
 * Sorts event by their start day so they're in order
 * This is so event collisions can correctly be calculated
 *
 * @function eventSort
 * @param {Array}
 * @return {Array}
 */
export const eventSort = array => array.sort((a, b) => {
    a = a.start;
    b = b.start;

    if (a < b) { return -1; }
    if (a > b) { return 1; }
    return 0;
});


/**
 * Checks if any of the events end before they start
 * Returns truthy if so
 *
 * @function eventsEndBeforeStart
 * @param {Array}
 * @return {Boolean}
 */
export const eventsEndBeforeStart = array => some(array, (obj) => obj.end <= obj.start);

/**
 * Checks if a value is odd or not
 *
 * @function isOdd
 * @param {Number}
 * @return {Boolean}
 */
export const isOdd = value => value % 2 === 1;
