'use strict';

import { eventsEndBeforeStart, eventSort } from 'utils/filter';

/**
 * Validates and sorts the events
 *
 * @middleware eventValidation
 */
const eventValidation = store => next => action => {
    // If action isn't 'ADD_EVENTS', continue flow
    if (action.type !== 'ADD_EVENTS') {
        return next(action);
    }

    if (eventsEndBeforeStart(action.events)) {
        return alert('Event Connot End Before Start');
    }

    // Lets make sure the events are in order
    action.events = eventSort(action.events);

    // Continue flow
    next(action);
};

export default eventValidation;
