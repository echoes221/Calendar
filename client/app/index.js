'use strict';

import './styles/appIndex.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { times } from 'constants/events';
import Calendar from 'components/Calendar';
import EventCard from 'components/EventCard';
import { positionEvents } from 'utils/position';

class App extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        const { events } = this.props;

        return (
            <div className = 'appWrapper'>
                <Calendar times = {times}>
                    {(rect) => {
                        return positionEvents(events, rect).map((eventRect, index) => (
                            <EventCard
                                key = {index}
                                {...eventRect}
                            />
                        ));
                    }}
                </Calendar>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    events: state.events
});

export default connect(
    mapStateToProps
)(App);
