'use strict';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import events from 'reducers/events';
import eventValidation from 'middleware/eventValidation';

const store = createStore(combineReducers({
    events
}), applyMiddleware(eventValidation));

export default store;
